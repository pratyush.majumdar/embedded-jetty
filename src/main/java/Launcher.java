import java.awt.Desktop;
import java.net.URI;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.webapp.Configuration;
import org.eclipse.jetty.webapp.WebAppContext;

public class Launcher {

	static int port = 8080;
	static String path = "/";

	public static void main(String args[]) {
		System.out.println("Starting Jetty Server on port " + port + " at path : " + path);
		Server server = new Server(port);

		WebAppContext webapp = new WebAppContext();
		webapp.setServer(server);
		webapp.setContextPath(path);
		webapp.setParentLoaderPriority(true);

		webapp.setDescriptor("WEB-INF/web.xml");
		webapp.setResourceBase("src/main/webapp");
		webapp.setParentLoaderPriority(true);

		Configuration.ClassList classlist = Configuration.ClassList.setServerDefault(server);
		classlist.addBefore("org.eclipse.jetty.webapp.JettyWebXmlConfiguration",
				"org.eclipse.jetty.annotations.AnnotationConfiguration");

		webapp.setAttribute("org.eclipse.jetty.server.webapp.ContainerIncludeJarPattern",
				".*/[^/]*servlet-api-[^/]*\\.jar$|.*/javax.servlet.jsp.jstl-.*\\.jar$|.*/[^/]*taglibs.*\\.jar$");

		server.setHandler(webapp);

		try {
			server.start();
			System.out.println("Jetty Server successfully started !!!");

			startBrowser();
			server.join();
		} catch (Exception e) {
			System.out.println("Error while starting Jetty Server : " + e.getMessage());
		}
	}

	private static void startBrowser() {
		if (Desktop.isDesktopSupported()) {
			try {
				Desktop.getDesktop().browse(new URI("http://localhost:" + port + path));
			} catch (Exception e) {
				System.out.println("Error while starting Browser : " + e.getMessage());
			}
		}
	}
}
