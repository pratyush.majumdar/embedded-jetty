# Project Title

Embedded Jetty as a Self Executable Web Archive

## Authors

Pratyush Majumdar <pratyush.majumdar@gmail.com>

## Built With

* Maven
* Jetty
* Eclipse

## License

This project is licensed under the MIT License